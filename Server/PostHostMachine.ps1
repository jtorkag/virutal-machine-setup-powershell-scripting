# Host Machine Script for Big5 Worldlink Integration Project
# Author: Jaime Torres
# 
#
#================================
#

# Clear Error Log
$error.clear()

# Get VM name
$vmData = get-vm
$vmName = $($vmData.name)

#Delete resources used
NET SHARE PS1 /delete

Remove-Item C:\PS1\InVM.ps1
Remove-Item C:\PS1\TaskCred.ps1
Remove-Item C:\PS1\RunInVM.lnk
Remove-Item C:\PS1


# Set to correct VMSwitch
Get-VM $vmName | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "Virtual Switch External"
# Remove temp Switch 
Remove-VMSwitch "Internal Switch Temp" -force
 
#Create error log if any present.
if ($error -ne ""){
Add-Content F:\Server\ErrorLog.txt $error
ii F:\Server\ErrorLog.txt
}
