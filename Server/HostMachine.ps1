# Host Machine Script for Big5 Worldlink Integration Project
# Author: Jaime Torres
# 
#
#================================
#

# Clear Error Log
$error.clear()

# Toggle Numlock on if off
if( [console]::NumberLock -eq $false){
$wsh = New-Object -ComObject WScript.Shell
$wsh.SendKeys('{NUMLOCK}')
}

# Prompt for store number.
$store = Read-Host -Promt 'Enter Store Number'
$store = [int]$store

# Disable IPv6.
Disable-NetAdapterBinding -Name "vEthernet (Virtual Switch External)" -ComponentID ms_tcpip6

# Set BackOffice URL
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\Users\Public\Desktop\Back Office.lnk")
$shortcutPath = [char]34 + "C:\Program Files\Internet Explorer\iexplore.exe\" + [char]34
$shortcutArgument = " http://sv00"+$([string]$store).padleft(3,[char]48)+"-orpos:9101/backoffice"
$Shortcut.TargetPath = $shortcutPath
$Shortcut.Arguments = $shortcutArgument
$Shortcut.IconLocation = "C:\Program Files\Internet Explorer\iexplore.exe,2"
$Shortcut.WorkingDirectory = "C:\Program Files\Internet Explorer"
$Shortcut.Save()

# add share file command to clipboard
"\\" + $env:computername.ToLower() + "\ps1" | clip.exe

# Determine store IP formula.

If ($store -le 10){

# Set computer name.
$newName = $([string]$store).padleft(3,[char]48)
Rename-Computer -NewName $newName

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -IPAddress x.$store.y -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.$store.y
Set-DnsClientServerAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -ServerAddresses x.y.z.a, x.y.z.a,x.y.z.a

}
If ($store -gt 10 -And $store -le 20){

# Set computer name.
$newName =$store
Rename-Computer -NewName $newName

# Fix math for stores above 10.
$store -= 10 

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -IPAddress x.$store.y -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.$store.y
Set-DnsClientServerAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -ServerAddresses  x.y.z.a, x.y.z.a,x.y.z.a

$store += 250
}
If ($store -gt 30){

# Set computer name.
$newName = $store
Rename-Computer -NewName $newName

# Fix math for stores above 30.
$store -= 30 

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -IPAddress x.$store.y -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.$store.y
Set-DnsClientServerAddress -InterfaceAlias "vEthernet (Virtual Switch External)" -ServerAddresses  x.y.z.a, x.y.z.a,x.y.z.a

$store += 30
}
# Create Shared Folder with files inside
New-Item "C:\PS1" -type directory
NET SHARE PS1=C:\PS1 /GRANT:Everyone`,FULL
Copy-Item F:\Server\InVM.ps1 C:\PS1
Copy-Item F:\Server\TaskCred.ps1 C:\PS1
Copy-Item F:\Server\RunInVM.lnk C:\PS1

# Set to temporary internal switch to share folder in VM
New-VMSwitch "Internal Switch Temp" -SwitchType Internal
Get-VM "svmName" | Get-VMNetworkAdapter | Connect-VMNetworkAdapter -SwitchName "Internal Switch Temp"

# Rename VM
$vmName =$([string]$store).padleft(3,[char]48)
Rename-VM "vmName" -NewName $vmName

# Start VM
Start-VM -Name $vmName

# Connect to VM
vmconnect localhost $vmName

# Open up Printers control
control /name Microsoft.devicesandprinters

#Create error log if any present.
if ($error -ne ""){
Add-Content F:\Server\ErrorLog.txt $error
ii F:\Server\ErrorLog.txt
}

