# Inside VM Script for Big5 Worldlink Integration Project
# Author: Jaime Torres
# 
#
#================================
#

# Clear Error Log
$error.clear()

# Prompt for store number.
$store = Read-Host -Promt 'Enter Store Number'
$store = [int]$store

# Disable IPv6.
Disable-NetAdapterBinding -Name "Ethernet" -ComponentID ms_tcpip6
### Fix name

# Enter store data to prop files for batch script.
$vmPropFile = "#############################################
#User require parameters input change
#############################################

newHostName="$([string]$store).padleft(3,[char]48)+"

newStoreID="+$([string]$store).padleft(3,[char]48)+"

newEmailStoreID="+$([string]$store).padleft(3,[char]48)+"
"
Remove-Item C:\path\UserInputParameter.properties
Add-Content C:\path\UserInputParameter.properties $vmPropFile
### correct Location 

# Fix credentials and disable service
$username = $env:computername + "\compName"
$secpasswd = ConvertTo-SecureString "password" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential($username, $secpasswd)

Z:\TaskCred.ps1 "\service_Location" $mycreds
Z:\TaskCred.ps1 "\service_Location" $mycreds

Disable-ScheduledTask -TaskName "\service_Location"

# Determine if store above or below 10 for IP formula.
If ($store -le 10)
{

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceIndex 12 -IPAddress x.y.$store.z -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.y.$store.z
Set-DnsClientServerAddress -InterfaceIndex 12 -ServerAddresses x.y.z.a,x.y.z.a,x.y.z.a

}
If ($store -gt 10 -And $store -le 20)
{
$store -= 20 

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceIndex 12 -IPAddress x.y.$store.z -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.y.$store.z
Set-DnsClientServerAddress -InterfaceIndex 12 -ServerAddresses x.y.z.a,x.y.z.a,x.y.z.a

}
If ($store -gt 20)
{
$store -= 30 

# Set Ip, Subnet, Gateway, and 3 DNS Servers.
New-NetIPAddress -InterfaceIndex 12 -IPAddress x.y.$store.z -AddressFamily IPv4 -PrefixLength 28 -DefaultGateway x.y.$store.z
Set-DnsClientServerAddress -InterfaceIndex 12 -ServerAddresses x.y.z.a,x.y.z.a,x.y.z.a

$store += 30
}

## Open folder with batch files
ii C:\path\
## Begin net config
Invoke-Item -Path "C:\path\Config.lnk"

#Create error log if any present.
if ($error -ne ""){
Add-Content C:\Users\user\Desktop\ErrorLog.txt $error
ii C:\Users\user\Desktop\ErrorLog.txt
}
