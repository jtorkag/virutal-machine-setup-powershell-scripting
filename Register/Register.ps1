# Register Script for Big5 Worldlink Integration Project
# Author: Jaime Torres
# 
#
#================================
#

# Clear Error Log
$error.clear()

# Prompt for Store number
$store = Read-Host -Promt 'Enter Store Number'
$store = [int]$store
# Prompt for Register number
$reg = Read-Host -Promt 'Enter Register Number'
$reg = [int]$reg

# Enable Administrator Account
net user administrator /active:yes

# Initialize values
$regIP = 10 + $reg
$leadIP = 0
$storeIP = 0

# Import store info from csv
$storeValues = import-csv D:\register\storeinfoall.txt | where-object {$_.store -eq $store}
$storeFull = $($storeValues.StoreFull)
$accountNumber = $($storeValues.TenderStoreBankAccountNumber)
$merchantNumber = $($storeValues.MerchantNumber)
$taxRate = $($storeValues.TaxRate)
$address =[char]34 + $($storeValues.StreetAddress) + [char]34
$state = $($storeValues.STATE)
$cityFull = [char]34 + $($storeValues.CITY) + " " +$storeValues.State + "," + $storeValues.ZIP + [char]34
$phoneNumber = [char]34 + $($storeValues.PublicLine)

# Update values with correct information
If ($store -le 10){
$leadIP = 1
$storeIP = $store
}
If ($store -gt 10 -And $store -le 20){
$leadIP = 2
$storeIP = $storeFull - 10
}

#working but irrelevant, no data for stores above 30

If ($store -gt 30){
$leadIP = 3
$storeIP = $storeFull - 30
}


#Create correct properties file
$regPropFile = "#############################################
#User require parameters input change
#############################################
#Config host name for new store server

newHostName="+$storeFull+"-ORPOS

storeServerIP="+$leadIP+"."+$storeIP+"


newStoreID=1"+$storeFull+"


newWorkstationID= 1 "+$reg+"

newVeriFoneHostName=10."+$leadIP+"."+$storeIP+"."+$regIP+"

newLogoLine1="+$address+"
newLogoLine2="+$cityFull+"
newLogoLine3="+$phoneNumber+"""

StoreStateProvince=US_"+$state+"

TenderStoreBankAccountNumber="+$accountNumber+"

TenderStoreBankName="+[char]34+"Bank Name" +[char]34+"

MerchantNumber="+$merchantNumber+"

TaxRate="+$taxRate+"
"

# Replace old properties file
Remove-Item C:\Path\UserInputParameter.properties
Add-Content C:\Path\UserInputParameter.properties $regPropFile

# Open batch and prop file location
ii C:\path

#Create error log if any present.
if ($error -ne ""){
Add-Content D:\Register\ErrorLog.txt $error
ii D:\Register\ErrorLog.txt
}
